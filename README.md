# EEPROM Programmer #

EEPROM Programmer firmware based on arduino nano and i8255 as extender.
Currently it supports 3 chips SST28SF040, W27C010, W27C512. 

Possibly it supports other chips with the same pinout and voltage levels.

[Schematics](https://easyeda.com/vitalian1980/EEPROM-Programmer)

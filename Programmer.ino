/*
  Parallel EEPROM Programmer

  Hardware: https://oshwlab.com/vitalian1980/EEPROM-Programmer
*/

#define POSITIVE_RESULT '+'
#define NEGATIVE_RESULT '-'

// Data bus pins
#define DATA0 8
#define DATA1 9
#define DATA2 2
#define DATA3 3
#define DATA4 4
#define DATA5 5
#define DATA6 6
#define DATA7 7

#define RD 11
#define WR 12
#define CS55 10
#define DB_DELAY 2
#define DELAY500NS() __asm("nop");__asm("nop");__asm("nop");__asm("nop");__asm("nop");

#define CSEEPROM 13
// High voltage regulator control pins
// HV12 == 1, HV14 == 0  -> VPP = 12 V
// HV12 == 0, HV14 == 1  -> VPP = 14 V
// HV12 == 1, HV14 == 2  -> VPP = 25 V
#define HV12 A2  
#define HV14 A3
// Number in I55 A=0..7 B=8..15 C=16..23
#define VPP_A18 23
#define VPP_A9 22
#define VPP_OE 21
#define VPP_A11 20
#define VPP_A15 19
#define DIP28_VCC_PIN 17
#define DIP24_VCC_PIN 13

// Use direct assess to ports instead of using slow arduino functions digitalWrite() and digitalRead()
#define DIRECT_ACCESS 1 


unsigned char EEPROM_read(unsigned long addr);
void EEPROM_program_byte(unsigned long addr, unsigned char data);
void EEPROM_W27C512_program_byte(unsigned long addr, unsigned char data);
void EEPROM_W27C512_erase_chip();
void EEPROM_W27C512_read_id();

unsigned char EEPROM_2716_read(unsigned long addr);
void EEPROM_2716_program_byte(unsigned long addr, unsigned char data);

void EEPROM_W27C010_program_byte(unsigned long addr, unsigned char data);
void EEPROM_W27C010_erase_chip();
void EEPROM_W27C010_read_id();

void EEPROM_SST28SF040_unlock();
void EEPROM_SST28SF040_lock();
void EEPROM_SST28SF040_program_byte(unsigned long addr, unsigned char data);
void EEPROM_SST28SF040_erase_chip();
void EEPROM_SST28SF040_read_id();

void EEPROM_SST39SF040_program_byte(unsigned long addr, unsigned char data);
void EEPROM_SST39SF040_erase_chip();
void EEPROM_SST39SF040_read_id();

void EEPROM_UV_program_byte(unsigned long addr, unsigned char data);


typedef struct {
  char * name;
  unsigned long size;
  unsigned char write_voltage;
  unsigned char clear_voltage;
  void (*unlock)();
  void (*lock)();
  long Vpp_pin;
  unsigned char package_size;
  void (*program_byte)(unsigned long addr, unsigned char data);
  void (*erase_chip)();
  void (*read_chip_id)();
  unsigned char (*read_data)(unsigned long addr);
  unsigned int write_pulse_width_us;
  
} ROM_CHIP;

ROM_CHIP CHIPS_LIST[] = {
  {"NONE", 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL},
  {"2716", (2 * 1024UL), 25, 0, NULL, NULL, VPP_A11, 24, EEPROM_2716_program_byte, NULL, NULL, EEPROM_2716_read, 30000},
  {"2732", (4 * 1024UL), 25, 0, NULL, NULL, VPP_OE, 24, EEPROM_UV_program_byte, NULL, NULL, EEPROM_read, 100},
  {"2764", (8 * 1024UL), 25, 0, NULL, NULL, VPP_A15, 24, EEPROM_UV_program_byte, NULL, NULL, EEPROM_read, 100},
  {"27128", (16 * 1024UL), 25, 0, NULL, NULL, VPP_A15, 28, EEPROM_UV_program_byte, NULL, NULL, EEPROM_read, 100},
  {"27256", (32 * 1024UL), 25, 0, NULL, NULL, VPP_A15, 28, EEPROM_UV_program_byte, NULL, NULL, EEPROM_read, 100},
  {"27512", (64 * 1024UL), 12, 0, NULL, NULL, VPP_OE, 28, EEPROM_UV_program_byte, NULL, EEPROM_W27C512_read_id, EEPROM_read, 100},
  {"W27C512", (64 * 1024UL), 12, 14, NULL, NULL, VPP_OE, 28, EEPROM_W27C512_program_byte, EEPROM_W27C512_erase_chip, EEPROM_W27C512_read_id, EEPROM_read, 100},
  {"W27C010", (128 * 1024UL), 12, 14, NULL, NULL, VPP_A18, 32, EEPROM_W27C010_program_byte, EEPROM_W27C010_erase_chip, EEPROM_W27C010_read_id, EEPROM_read, 100},  
  {"W27C020", (256 * 1024UL), 12, 14, NULL, NULL, VPP_A18, 32, EEPROM_W27C010_program_byte,  EEPROM_W27C010_erase_chip, EEPROM_W27C010_read_id, EEPROM_read, 100},  
  {"SST28SF040", (512 * 1024UL), 5, 5, EEPROM_SST28SF040_unlock, EEPROM_SST28SF040_lock, 0, 32, EEPROM_SST28SF040_program_byte, EEPROM_SST28SF040_erase_chip, EEPROM_SST28SF040_read_id, EEPROM_read, 0},  
  {"SST39SF040", (512 * 1024UL), 5, 5, NULL, NULL, 0, 32, EEPROM_SST39SF040_program_byte, EEPROM_SST39SF040_erase_chip, EEPROM_SST39SF040_read_id, EEPROM_read, 0},  
  {"A29040", (512 * 1024UL), 5, 5, NULL, NULL, 0, 32, NULL, NULL, NULL, EEPROM_read, 0},
};

const int CHIPS_LIST_SIZE = sizeof(CHIPS_LIST)/sizeof(ROM_CHIP);
ROM_CHIP * selected_chip = NULL; //

#define DB_OUTPUT(data) DDRD = DDRD | B11111100; DDRB = DDRB | B00000011; \
  PORTB = (PORTB & B11111100) | (data & B00000011); \
  PORTD = (PORTD & B00000011) | (data & B11111100);

#define DB_INPUT() DDRD = DDRD & B00000011; DDRB = DDRB & B11111100; \
  PORTB = PORTB | B00000011; PORTD = PORTD | B11111100;

#define DB_READ() (PIND & B11111100) | (PINB & B00000011);

#define I55_SELECT() PORTB = PORTB & B11111011;
#define I55_UNSELECT() PORTB = PORTB | B00000100;
#define EEPROM_SELECT() PORTB = PORTB & B11011111;
#define EEPROM_UNSELECT() PORTB = PORTB | B00100000;

#if DIRECT_ACCESS

#define SETBIT(PORT, BIT) (PORT |= (1<<BIT))
#define CLRBIT(PORT, BIT) (PORT &= ~(1<<BIT))
#define _WRITE(PORT, BIT, LEVEL) (LEVEL == HIGH ? SETBIT(PORT, BIT) : CLRBIT(PORT, BIT))

// overload digitalWrite for faster operation
#define digitalWrite(PIN, LEVEL) if(PIN>=14) {_WRITE(PORTC, (PIN-14), LEVEL);} \
      else if(PIN>=8) {_WRITE(PORTB, (PIN-8), LEVEL);} \
      else {_WRITE(PORTD, PIN, LEVEL);}

#define GETBIT(PORT, BIT) (PORT & (1<<BIT))
// overload digitalRead for faster operation
#define digitalRead(PIN) ((PIN>=14) ? GETBIT(PORTC, (PIN-14)) :  \
      (PIN>=8) ? GETBIT(PORTB, (PIN-8)) : GETBIT(PORTD, PIN))      

#endif 

#define CMD_HELP 'h'
#define CMD_LIST 'l'
#define CMD_CHIP_ID 'i'
#define CMD_ERASE 'e'
#define CMD_WRITE 'w'
#define CMD_READ 'r'
#define CMD_READ_ALL 'a'
#define CMD_TEST 't'
#define CMD_SELECT 's'
#define CMD_VERIFY 'v'
#define CMD_DUMP 'd'
#define CMD_BLANK 'b'
#define CMD_SET_VOLTAGE 'o'

#define UPPERCASE(ch) ((ch) + ('A'-'a'))

#define INPUT_BUFFER_SIZE 128

void print_help()
{
  Serial.println();
  Serial.println(F("h - this Help"));
  Serial.println(F("l - show List of supported ROMs"));
  Serial.println(F("s <type> - Select ROM type"));
  Serial.println(F("i - get ROM chip Id (only for modern chips which support this feature)"));
  Serial.println(F("b - Blank check"));
  Serial.println(F("e - Erase entire ROM chip"));
  Serial.println(F(":xxxx... - write intel HEX line to ROM chip"));
  Serial.println(F("w <addr> <size> <data> - Write data into ROM. addr, size, data are HEX values"));  
  Serial.println(F("r <addr> <size> - Read data from ROM. addr, size are HEX values"));  
  Serial.println(F("d <addr> <size> - Dump data from ROM. addr, size are HEX values. Produces intel HEX output")); 
  Serial.println(F("a - dump All data")); 
  Serial.println(F("v <addr> <size> <crc16> - Verify data")); 
  Serial.println(F("o <voltage> - set vOltage (5, 12, 14, 25)"));    
}

//////////////////////
// Data Bus functions
//////////////////////
unsigned char DB_read()
{
  unsigned char ret;
  digitalWrite(RD, LOW);  
  DELAY500NS();
  
#if DIRECT_ACCESS 
  ret = DB_READ();
#else
  ret = digitalRead(DATA0) | (digitalRead(DATA1)<<1) | (digitalRead(DATA2)<<2) | (digitalRead(DATA3)<<3) |
        (digitalRead(DATA4)<<4) | (digitalRead(DATA5)<<5) | (digitalRead(DATA6)<<6) | (digitalRead(DATA7)<<7);
#endif  
  digitalWrite(RD, HIGH); 
 
  return ret;
}

void DB_write_start(unsigned char data)
{
#if DIRECT_ACCESS   
  DB_OUTPUT(data);
#else
  pinMode(DATA0, OUTPUT);
  pinMode(DATA1, OUTPUT);
  pinMode(DATA2, OUTPUT);    
  pinMode(DATA3, OUTPUT);
  pinMode(DATA4, OUTPUT);
  pinMode(DATA5, OUTPUT); 
  pinMode(DATA6, OUTPUT);
  pinMode(DATA7, OUTPUT);  

  digitalWrite(DATA0, data & 1);
  digitalWrite(DATA1, data & 2);
  digitalWrite(DATA2, data & 4);
  digitalWrite(DATA3, data & 8);
  digitalWrite(DATA4, data & 16);
  digitalWrite(DATA5, data & 32);
  digitalWrite(DATA6, data & 64);
  digitalWrite(DATA7, data & 128);
#endif 

  digitalWrite(WR, LOW);
}

void DB_write_finish()
{  
  digitalWrite(WR, HIGH); 
   
#if DIRECT_ACCESS   
  DB_INPUT();
#else
  pinMode(DATA0, INPUT_PULLUP);
  pinMode(DATA1, INPUT_PULLUP);
  pinMode(DATA2, INPUT_PULLUP);    
  pinMode(DATA3, INPUT_PULLUP);
  pinMode(DATA4, INPUT_PULLUP);
  pinMode(DATA5, INPUT_PULLUP); 
  pinMode(DATA6, INPUT_PULLUP);
  pinMode(DATA7, INPUT_PULLUP); 
#endif 
}

void DB_write(unsigned char data, unsigned int pulse_width_us=0)
{
  DB_write_start(data);
  if(!pulse_width_us)
  {
    DELAY500NS();  
  }
  else
  {
    delayMicroseconds(pulse_width_us);
  }
  DB_write_finish();
}

void DB_write_ms(unsigned char data, unsigned int timeout=DB_DELAY)
{
  DB_write_start(data);
  delay(timeout);
  DB_write_finish();
}

static unsigned char I55_registers[4]; // Cache for i8255 ports

//////////////////////
// Setting address via i8255
//////////////////////
void I55_select()
{
#if DIRECT_ACCESS
  I55_SELECT();
#else
  digitalWrite(CS55, LOW);
#endif
}

void I55_unselect()
{
#if DIRECT_ACCESS
  I55_UNSELECT();
#else  
  digitalWrite(CS55, HIGH);
#endif
}

void I55_addr(unsigned char addr)
{
#if DIRECT_ACCESS
  PORTC = (PORTC & B11111100) | (addr & 3);
#else    
  digitalWrite(A0, addr & 1);
  digitalWrite(A1, addr & 2);  
#endif
}

void I55_write(unsigned char addr, unsigned char data, bool no_cache = false)
{
  if(no_cache || (I55_registers[addr] != data))
  {
    I55_select();
    I55_addr(addr);
    DB_write(data);
    I55_unselect();
    I55_registers[addr] = data;
  }  
}

unsigned char I55_read(unsigned char addr)
{
  unsigned char ret;
  I55_select();
  I55_addr(addr);
  ret = DB_read();
  I55_unselect(); 
  return ret;
}

//////////////////////
// Activate CS ROM pin
//////////////////////
void EEPROM_select()
{
#if DIRECT_ACCESS
  EEPROM_SELECT();
#else  
  digitalWrite(CSEEPROM, LOW);
#endif  
}

void EEPROM_unselect()
{
#if DIRECT_ACCESS
  EEPROM_UNSELECT();
#else  
  digitalWrite(CSEEPROM, HIGH);
#endif  
}

// Throws this error in many places
void print_error_not_selected()
{
  Serial.println(F("ROM chip is not selected!"));
}

// Set special pins like Vpp, Vcc
unsigned char EEPROM_set_service_pin(unsigned char pin_number)
{
  unsigned long addr = (((unsigned long)I55_registers[2] << 16) | ((unsigned long)I55_registers[1] << 8) 
    | (unsigned long)I55_registers[0]) | (1UL << pin_number);
      
  I55_write(2, (unsigned char) (addr>>16)); 
  I55_write(1, (unsigned char) (addr>>8)); 
  I55_write(0, (unsigned char) addr);   
}

// Reset special pins like Vpp, Vcc
unsigned char EEPROM_reset_service_pin(unsigned char pin_number)
{
  unsigned long addr = (((unsigned long)I55_registers[2] << 16) | ((unsigned long)I55_registers[1] << 8) 
    | (unsigned long)I55_registers[0]) & ~(1UL << pin_number);
      
  I55_write(2, (unsigned char) (addr>>16)); 
  I55_write(1, (unsigned char) (addr>>8)); 
  I55_write(0, (unsigned char) addr);   
}

// merges service bits of port and address
void EEPROM_set_address(unsigned long addr)
{
  unsigned long mask;

  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }

  mask = selected_chip->size - 1;
  
  if(addr >= selected_chip->size)
  {
    Serial.print(F("Address 0x"));
    Serial.print(addr, HEX);
    Serial.println(F(" is too big for the chip!"));
    return;
  }

  addr &= mask;
  addr |= ((((unsigned long)I55_registers[2] << 16) | ((unsigned long)I55_registers[1] << 8) 
          | (unsigned long)I55_registers[0]) & ~mask);
        
  I55_write(2, (unsigned char) (addr>>16)); 
  I55_write(1, (unsigned char) (addr>>8)); 
  I55_write(0, (unsigned char) addr); 
}

// Generic reading function
unsigned char EEPROM_read(unsigned long addr)
{
  unsigned char ret;
  EEPROM_set_address(addr);
      
  EEPROM_select();
  ret = DB_read();
  EEPROM_unselect(); 
  return ret;
}

// only writes into ROM chip
void EEPROM_write(unsigned long addr, unsigned char data, unsigned int pulse_width_us=0)
{
  EEPROM_set_address(addr);
      
  EEPROM_select();
  DB_write(data, pulse_width_us);
  EEPROM_unselect(); 
}

void EEPROM_program_byte(unsigned long addr, unsigned char data)
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();  
    return;
  }
  // Enable HV PIN
  if(selected_chip->Vpp_pin)
  {
    EEPROM_set_service_pin(selected_chip->Vpp_pin);
  }
  
  EEPROM_write(addr, data, selected_chip->write_pulse_width_us);
  // Disable HV PIN 
  if(selected_chip->Vpp_pin)
  {
    EEPROM_reset_service_pin(selected_chip->Vpp_pin);
  }
}

void EEPROM_UV_program_byte(unsigned long addr, unsigned char data)
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();  
    return;
  }

  if(data == 0xFF)
  {
    return; 
  }

  for(char i = 0; i < 25; i++)
  {
    // Enable HV PIN
    if(selected_chip->Vpp_pin)
    {
      EEPROM_set_service_pin(selected_chip->Vpp_pin);
    }

    delayMicroseconds(2);    
    EEPROM_write(addr, data, selected_chip->write_pulse_width_us);
    // Disable HV PIN 
    if(selected_chip->Vpp_pin)
    {
      EEPROM_reset_service_pin(selected_chip->Vpp_pin);
    }
    delayMicroseconds(2);

    if(data == selected_chip->read_data(addr))
    {
      break;  
    }
  }
}

//////////////////////
// M2716
// Needs inverted levels on CS pin during programming 
//////////////////////
void EEPROM_2716_program_byte(unsigned long addr, unsigned char data)
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();  
    return;
  }

  if(data == 0xFF)
  {
    return; 
  }

  for(char i = 0; i < 25; i++) // Trying several times to write data
  {
    EEPROM_select();
    // Enable HV PIN
    if(selected_chip->Vpp_pin)
    {
      EEPROM_set_service_pin(selected_chip->Vpp_pin);
    }
    EEPROM_set_address(addr);
    DB_write_start(data);
    delayMicroseconds(2);   

    EEPROM_unselect();
    delay(50);
    EEPROM_select();
    delayMicroseconds(2);  
    DB_write_finish();
    // Disable HV PIN 
    if(selected_chip->Vpp_pin)
    {
      EEPROM_reset_service_pin(selected_chip->Vpp_pin);
    }
    EEPROM_unselect();   
    if(data == selected_chip->read_data(addr))
    {
      break;  
    }
  }
}

unsigned char EEPROM_2716_read(unsigned long addr)
{
  unsigned result;
  EEPROM_set_service_pin(11); // A11/Vpp pin = HIGH  
  result = EEPROM_read(addr);
  EEPROM_reset_service_pin(11);
  return result;
}

//////////////////////
// SST28SF040
//////////////////////
void EEPROM_SST28SF040_unlock() // SST28SF040 sequence must be executed before writing
{
  // 1823H, 1820H, 1822H, 0418H, 041BH, 0419H, 041AH
  EEPROM_read(0x1823UL);
  EEPROM_read(0x1820UL);
  EEPROM_read(0x1822UL);
  EEPROM_read(0x0418UL);
  EEPROM_read(0x041BUL);
  EEPROM_read(0x0419UL);
  EEPROM_read(0x041AUL);
}

void EEPROM_SST28SF040_lock() // SST28SF040 sequence
{
  // 1823H, 1820H, 1822H, 0418H, 041BH, 0419H, 040AH
  EEPROM_read(0x1823UL);
  EEPROM_read(0x1820UL);
  EEPROM_read(0x1822UL);
  EEPROM_read(0x0418UL);
  EEPROM_read(0x041BUL);
  EEPROM_read(0x0419UL);
  EEPROM_read(0x040AUL);
}

void EEPROM_SST28SF040_reset()
{
  EEPROM_write(0, 0xFF);
}

void EEPROM_SST28SF040_read_id()
{
  EEPROM_write(0, 0x90);
  Serial.print(EEPROM_read(0), HEX);
  Serial.print(F(" "));  
  Serial.println(EEPROM_read(1), HEX);
  EEPROM_SST28SF040_reset();
}
void EEPROM_SST28SF040_erase_chip()
{
  EEPROM_write(0, 0x30);
  delayMicroseconds(DB_DELAY);
  EEPROM_select();  
  DB_write(0x30);  
  while(DB_read() != 0xFF);
  EEPROM_unselect();
}

void EEPROM_SST28SF040_erase_sector(unsigned long addr)
{
  EEPROM_write(addr, 0x20);
  delayMicroseconds(DB_DELAY);
  EEPROM_select();  
  DB_write(0xD0);  
  while(DB_read() != 0xFF);
  EEPROM_unselect();
}

void EEPROM_SST28SF040_program_byte(unsigned long addr, unsigned char data)
{
  EEPROM_write(addr, 0x10);
  delayMicroseconds(DB_DELAY);
  EEPROM_select();  
  DB_write(data);  
  while((DB_read() & 0x80) != (data & 0x80));
  EEPROM_unselect();
}

//////////////////////
// SST39SF040
//////////////////////
void EEPROM_SST39SF040_read_id()
{
  EEPROM_write(0x5555, 0xAA);
  EEPROM_write(0x2AAA, 0x55);  
  EEPROM_write(0, 0x90);
  Serial.print(EEPROM_read(0), HEX);
  Serial.print(F(" "));  
  Serial.println(EEPROM_read(1), HEX);
  EEPROM_write(0, 0xF0);
}

void EEPROM_SST39SF040_erase_chip()
{
  EEPROM_write(0x5555, 0xAA);
  EEPROM_write(0x2AAA, 0x55);  
  EEPROM_write(0x5555, 0x80);
  EEPROM_write(0x5555, 0xAA);
  EEPROM_write(0x2AAA, 0x55);  
  EEPROM_write(0x5555, 0x10);

  while(DB_read() != 0xFF);
  EEPROM_unselect();
}


void EEPROM_SST39SF040_program_byte(unsigned long addr, unsigned char data)
{

  EEPROM_write(0x5555, 0xAA);
  EEPROM_write(0x2AAA, 0x55);  
  EEPROM_write(0x5555, 0xA0);
  EEPROM_write(addr, data);

  DB_write(data);  
  while((DB_read() & 0x80) != (data & 0x80));
}

//////////////////////
// W27C0x0 
//////////////////////
void EEPROM_W27C010_read_id()
{
  I55_write(2, VPP_A9); 
  I55_write(1, 0); 
  I55_write(0, 0); 
      
  EEPROM_select();
  Serial.print(DB_read(), HEX);
  Serial.print(" ");  
  EEPROM_unselect();  
  I55_write(0, 1); 
  EEPROM_select();   
  Serial.println(DB_read(), HEX);

  EEPROM_unselect(); 
  I55_write(2, 0);
}

void EEPROM_W27C010_erase_chip()
{
  EEPROM_set_voltage(14);
  EEPROM_set_service_pin(VPP_A18);
  EEPROM_set_service_pin(VPP_A9);  
      
  EEPROM_select();
  DB_write_ms(0xFF, 100);
  EEPROM_unselect(); 
  I55_write(2, 0); 
  
  EEPROM_set_voltage(12);
}

void EEPROM_W27C010_program_byte(unsigned long addr, unsigned char data)
{
  I55_write(2, (unsigned char) (((addr>>16) & 0x0F) | VPP_A18)); 
  I55_write(1, (unsigned char) ((addr>>8) & 0xFF)); 
  I55_write(0, (unsigned char) (addr & 0xFF)); 
      
  EEPROM_select();
  DB_write(data, 100);
  EEPROM_unselect(); 

  I55_write(2, (unsigned char) ((addr>>16) & 0x0F)); 
}


//////////////////////
// W27C512
//////////////////////
void EEPROM_W27C512_read_id()
{
  I55_write(0, 0); 
  I55_write(1, 0); 
  I55_write(2, VPP_A9 | DIP28_VCC_PIN); 
      
  EEPROM_select();
  Serial.print(DB_read(), HEX);
  Serial.print(" ");  
  EEPROM_unselect();  
  I55_write(0, 1); 
  EEPROM_select();   
  Serial.println(DB_read(), HEX);

  EEPROM_unselect(); 
  I55_write(2, DIP28_VCC_PIN);
}

void EEPROM_W27C512_erase_chip()
{
  EEPROM_set_voltage(14);
  I55_write(2, VPP_OE | VPP_A9 | DIP28_VCC_PIN); 
  I55_write(1, 0); 
  I55_write(0, 0); 

  DB_write_start(0xFF);
  EEPROM_select();
  delay(100);
  EEPROM_unselect(); 
  DB_write_finish();  
  I55_write(2, DIP28_VCC_PIN); 
  
  EEPROM_set_voltage(12);
}

void EEPROM_W27C512_write(unsigned long addr, unsigned char data)
{
  EEPROM_set_service_pin(VPP_OE);

  DB_write_start(data);    
  EEPROM_select();
  delayMicroseconds(100);
  EEPROM_unselect(); 
  DB_write_finish();

  EEPROM_reset_service_pin(VPP_OE); 
}

void EEPROM_W27C512_program_byte(unsigned long addr, unsigned char data)
{
  EEPROM_W27C512_write(addr, data);
}


//////////////////////
// Writes the data array into ROM
//////////////////////
void EEPROM_program(unsigned long addr, unsigned char * data, unsigned int size)
{
  unsigned int i;

  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }

  if(selected_chip->unlock != NULL)
  {
    selected_chip->unlock();
  }

  // Programming cycle
  for(i=0; i<size; i++)
  {
    selected_chip->program_byte(addr+i, data[i]);
  }

  if(selected_chip->lock != NULL)
  {
    selected_chip->lock();
  }  
  // verify
  for(i=0; i<size; i++)
  {
    if(selected_chip->read_data(addr+i) != data[i])
    {
      Serial.println(F("Verification failed!"));     
      break;  
    }
  } 
}

//////////////////////
// Writes an array of hex pairs into ROM
//////////////////////
void EEPROM_program_hex(unsigned long addr, unsigned char * data, unsigned int size)
{
  Serial.println(F("Not defined yet")); 
}

void EEPROM_verify()
{
  Serial.println(F("Not defined yet"));  
}

void EEPROM_check_blank()
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }
  
  for(unsigned long i=0; i < selected_chip->size; i++)
  {
    if(selected_chip->read_data(i) != 0xFF)
    {
      Serial.println(F("The chip is not empty!"));     
      break;  
    }

    if(!(i % 1024))
    {
      Serial.print(i / 1024);
      Serial.println(F(" kB"));
    }    
  } 
  Serial.println(F("OK"));  
}

// Just dumps all ROM memory
void EEPROM_read_all()
{
  unsigned long i;

  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }
  
  for(i=0; i < selected_chip->size; i++)
  {
    //*
     if(!(i % 256))
    {
      Serial.println();
    }   
    if(!(i % 16))
    {
      Serial.println();
      print_hex(i>>16);
      print_hex((i>>8) & 0xFF);
      print_hex(i & 0xFF);
      Serial.print(F(" "));           
    }
    print_hex(selected_chip->read_data(i));
    Serial.print(F(" "));  
    //*/ 
    
  }
  Serial.println(); 
}

void EEPROM_test()
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }

  selected_chip->erase_chip();

  for(unsigned long i = 0; i < selected_chip->size; i++)
  {
    selected_chip->program_byte(i, (unsigned char) (((i>>8)&0xF0) | (i&0xF)));
    if(!(i % 1024))
    {
      Serial.print(i / 1024);
      Serial.println(" kB");
    }
  }

}



void EEPROM_set_voltage(unsigned int voltage)
{
  switch(voltage)
  {
    case 12:
      digitalWrite(HV12, HIGH);
      digitalWrite(HV14, LOW);  
      break;

    case 14:
      digitalWrite(HV12, LOW);
      digitalWrite(HV14, HIGH);  
      break;
      
    case 25:
      digitalWrite(HV12, HIGH);
      digitalWrite(HV14, HIGH);  
      break;

    default: // 5V
      digitalWrite(HV12, LOW);
      digitalWrite(HV14, LOW);    
  }
  delay(500);
}

void print_chip(ROM_CHIP * chip, int id)
{
    Serial.print(id, HEX);  
    Serial.print(F("\t")); 
    Serial.print(chip->name);  
    Serial.print(F("\t")); 
    Serial.print(chip->size / 1024);  
    Serial.print(F("kB\t")); 
    Serial.print(chip->write_voltage);  
    Serial.println(F("V"));   
}

void EEPROM_list_chips()
{
  Serial.println(F("List of supported ROM chips")); 
  for(int i = 0; i < CHIPS_LIST_SIZE; i++)
  {
      print_chip(&CHIPS_LIST[i], i);
  }
}


void EEPROM_select_type(unsigned int type)
{
  if(type > 0 && type < CHIPS_LIST_SIZE)
  {
    selected_chip = &CHIPS_LIST[type];
  }
  else
  {
    selected_chip = NULL;
    print_error_not_selected();
    return;    
  }  

  EEPROM_set_voltage(selected_chip->write_voltage);

  switch(selected_chip->package_size)
  {
    case 24:
      EEPROM_reset_service_pin(DIP28_VCC_PIN);
      EEPROM_set_service_pin(DIP24_VCC_PIN);
      //Serial.println(F("24 pins"));
      break;

    case 28:
      EEPROM_reset_service_pin(DIP24_VCC_PIN);
      EEPROM_set_service_pin(DIP28_VCC_PIN);
      //Serial.println(F("28 pins"));
      break;

    default:
      EEPROM_reset_service_pin(DIP24_VCC_PIN);
      EEPROM_reset_service_pin(DIP28_VCC_PIN);
      //Serial.println(F("32 pins"));
  }
  Serial.println();
  Serial.print(selected_chip->name);  
  Serial.println(F(" selected"));
  print_chip(selected_chip, type);  
}

// the setup function runs once when you press reset or power the board
void setup() {

  pinMode(RD, OUTPUT);
  pinMode(WR, OUTPUT);
  pinMode(CS55, OUTPUT);
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(CSEEPROM, OUTPUT);
  pinMode(HV12, OUTPUT);
  pinMode(HV14, OUTPUT);  
  
  digitalWrite(RD, HIGH);
  digitalWrite(WR, HIGH);
  digitalWrite(CS55, HIGH);
  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(CSEEPROM, HIGH);  
  digitalWrite(HV12, LOW);
  digitalWrite(HV14, LOW);   
  
#if DIRECT_ACCESS   
  DB_INPUT()
#else
  pinMode(DATA0, INPUT_PULLUP);
  pinMode(DATA1, INPUT_PULLUP);
  pinMode(DATA2, INPUT_PULLUP);    
  pinMode(DATA3, INPUT_PULLUP);
  pinMode(DATA4, INPUT_PULLUP);
  pinMode(DATA5, INPUT_PULLUP); 
  pinMode(DATA6, INPUT_PULLUP);
  pinMode(DATA7, INPUT_PULLUP); 
#endif  

  Serial.begin(115200);

  I55_write(3, 128, true); // set mode 0 (OUTPUT) for all ports
  I55_write(0, 0, true); 
  I55_write(1, 0, true);
  I55_write(2, 0, true);

  EEPROM_select_type(0);

}

bool is_hex_digit(char digit)
{
    if((digit >= '0' && digit <= '9') 
      || (digit >= 'a' && digit <= 'f')
      || (digit >= 'A' && digit <= 'F'))
    {
      return true;  
    }
    return false;    
}

unsigned char parse_hex_digit(char digit)
{
    if(digit >= '0' && digit <= '9')
    {
      return digit - '0';  
    }
    else if(digit >= 'a' && digit <= 'f')
    {
      return digit - 'a' + 10;  
    }
    else if(digit >= 'A' && digit <= 'F')
    {
      return digit - 'A' + 10;  
    }
    return 0;    
}

unsigned char parse_2_hex_digits(char * buffer)
{
    return (parse_hex_digit(buffer[0])<<4) | parse_hex_digit(buffer[1]); 
}


void print_hex(unsigned char value)
{
  if(value < 16)
  {
    Serial.print('0');    
  }
  Serial.print(value, HEX);
}

// parses buffer till next non-hex symbol
unsigned long parse_hex_digits(char ** buffer, unsigned char * size)
{
  unsigned long ret = 0;
  unsigned char i = 0;
  
  //skip non digit chars
  for (; (i < *size) && !is_hex_digit((*buffer)[i]); i++)
    ; // cycle body omitted

  //parse a group ot digits
  for (; (i < *size) && is_hex_digit((*buffer)[i]); i++)
  {
    ret <<= 4;
    ret |= parse_hex_digit((*buffer)[i]);
  }

  *size -= i; // return number of read chars
  *buffer += i; // move buffer pointer
  
  return ret; 
}

// Parses Intel HEX line and writes it into ROM
void program_intel_hex_line(char * buffer, unsigned char size)
{
  if(selected_chip == NULL)
  {
    print_error_not_selected();
    return;
  }

  if(size < 10)
  {
    Serial.println(F("Too short line!"));
    return;
  }
  
  unsigned char data_len = parse_2_hex_digits(buffer);
  unsigned char * data = buffer;
  unsigned int addr = parse_2_hex_digits(buffer+2)<<8 | parse_2_hex_digits(buffer+4);
  unsigned char type = parse_2_hex_digits(buffer+6);
  unsigned char sum = data_len + (addr >> 8) + (addr & 0xFF) + type;
  unsigned char i;

  if(type == 4)
  {
    Serial.print(F("Set address bias: 0x"));
    unsigned int addr_bias = parse_2_hex_digits(buffer+8)<<8 | parse_2_hex_digits(buffer+10);
    print_hex((unsigned char)(addr_bias >> 8));
    print_hex((unsigned char)addr_bias);
    Serial.println();  
    
    return;
  }
  if(type != 0)
  {
    Serial.println(F("- Not a data line!"));
    return;
  }
  buffer += 8; size -=8;
  if(data_len * 2 > (size-2))
  {
    Serial.println(F("- Data length mismatch!"));
    return;    
  }
  for(i=0; i < data_len; i++, buffer += 2)
  {
    data[i] = parse_2_hex_digits(buffer);
    sum += data[i];
  }
  sum += parse_2_hex_digits(buffer);

  if(sum != 0)
  {
    Serial.println(F("- Checksum error!"));
    return;   
  }

  EEPROM_program(addr, data, data_len);
  Serial.println(POSITIVE_RESULT);
  
}

void parse_command(char * buffer, unsigned char size)
{
  byte incomingByte;
  unsigned long addr, len;

  if(size == 0)
  {
    return;  
  }
    
  switch(buffer[0])
  {
    case ':':
      program_intel_hex_line(++buffer, --size);
      break;

    case CMD_HELP:
    case UPPERCASE(CMD_HELP):
      print_help();
      break;
    
    case CMD_CHIP_ID:
    case UPPERCASE(CMD_CHIP_ID):
      selected_chip->read_chip_id();
      break;

    case CMD_LIST: 
    case UPPERCASE(CMD_LIST):
      EEPROM_list_chips();
      break;
    
    // Select EEPROM type
    case CMD_SELECT: 
    case UPPERCASE(CMD_SELECT):
      addr = parse_hex_digits(&(++buffer), &(--size));
      EEPROM_select_type(addr);
      break;

    case CMD_ERASE:
    case UPPERCASE(CMD_ERASE):
      if(selected_chip->erase_chip != NULL)
      {
        if(selected_chip->unlock != NULL)
        {
          selected_chip->unlock();
        }
        
        selected_chip->erase_chip(); // Erase
        
        if(selected_chip->lock != NULL)
        {
          selected_chip->lock();
        }
        
        Serial.println(F("The chip has been erased"));        
      }
      else
      {
        Serial.println(F("The chip cannot be erased!"));  
      }

      break;

    case CMD_BLANK:
    case UPPERCASE(CMD_BLANK):
      EEPROM_check_blank();
      break; 

    case CMD_VERIFY:
    case UPPERCASE(CMD_VERIFY):
      EEPROM_verify();
      break;  

    case CMD_WRITE:
    case UPPERCASE(CMD_WRITE):
      addr =  parse_hex_digits(&(++buffer), &(--size));
      len = (unsigned long) parse_hex_digits(&(++buffer), &(--size)); 

      EEPROM_program_hex(addr, (unsigned char *)buffer, size);
      Serial.println("OK");

      break;

    case CMD_READ:
    case UPPERCASE(CMD_READ):
      addr =  parse_hex_digits(&(++buffer), &(--size));
      len = (unsigned long) parse_hex_digits(&(++buffer), &(--size));      
      for(unsigned long i=0; i<len; i++)
      {
        if(!(i%16))
        {
          Serial.println();  
        }
        print_hex(selected_chip->read_data(addr++));
        Serial.print(F(" "));

      }
      break;

    case CMD_READ_ALL:
      EEPROM_read_all();
      Serial.println("OK");
      break;  

    case CMD_TEST:
      EEPROM_test();
      Serial.println("OK");
      break;    

  }
}

// the loop function runs over and over again forever

void loop() {
  static char buffer[INPUT_BUFFER_SIZE];
  static unsigned char size = 0;
  
  if (Serial.available() > 0) {
    // read the incoming byte:
    byte incomingByte = Serial.read();
    Serial.write(incomingByte);

    switch(incomingByte)
    {
      case '\n':
      case '\r':
        parse_command(buffer, size);
        size = 0;
        break;

      default:
        buffer[size++] = incomingByte;
        if(size >= INPUT_BUFFER_SIZE)
        {
            Serial.println(F("Input buffer overrun!"));
            size = 0;
        }
        
    }

  }
}
